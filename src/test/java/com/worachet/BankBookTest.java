package com.worachet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BankBookTest {
    @Test
    public void shouldWitdrawSuccess(){
        BookBank book = new BookBank("Worachet", 100);
        book.withdraw(50);
        assertEquals(50,book.balance,0.00001);
    }
    @Test
    public void shouldWitdrawOverBalance(){
        BookBank book = new BookBank("Worachet", 100);
        book.withdraw(150);
        assertEquals(100,book.balance,0.00001);
    }
    @Test
    public void shouldWitdrawNegativeNumber(){
        BookBank book = new BookBank("Worachet", 100);
        book.withdraw(-100);
        assertEquals(100,book.balance,0.00001);
    }
    @Test
    public void shouldDepositNegativeNumber(){
        BookBank book = new BookBank("Worachet", 100);
        book.deposit(-100);
        assertEquals(100,book.balance,0.00001);
}
@Test
public void shouldDepositSuccess(){
    BookBank book = new BookBank("Worachet", 100);
    book.deposit(50);
    assertEquals(150,book.balance,0.00001);
}
}
