package com.worachet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver(){
        Robot robot = new Robot("Robot", 'R', 0, Robot.Max_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.Max_Y, robot.getY());
    }
    @Test
    public void shouldUpNegative(){
        Robot robot = new Robot("Robot", 'R', 0, Robot.Min_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.Max_Y, robot.getY());
    }
    @Test
    public void shouldDownSuccess(){
        Robot robot = new Robot("Robot", 'R', 0, 0); 
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }
    @Test
    public void shouldUpSuccess(){
        Robot robot = new Robot("Robot", 'R', 0, 1); 
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
}
@Test
    public void shouldleftSuccess(){
        Robot robot = new Robot("Robot", 'R', 1, 0); 
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
}
@Test
    public void shouldrightSuccess(){
        Robot robot = new Robot("Robot", 'R', 0, 0); 
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
}
@Test
    public void shouldRightOver(){
        Robot robot = new Robot("Robot", 'R', Robot.Max_X, 0); 
        assertEquals(false, robot.right());
        assertEquals(Robot.Max_X, robot.getX());
}
@Test
    public void shouldLeftNegative(){
        Robot robot = new Robot("Robot", 'R', Robot.Min_X, 0); 
        assertEquals(false, robot.left());
        assertEquals(Robot.Min_X, robot.getX());
}
}