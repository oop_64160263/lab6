package com.worachet;

import javax.management.modelmbean.ModelMBean;

public class BookBank {
    // Attributes
    String name;
    double balance;
    public BookBank(String name,double balance){// Constructor
       this.name =name;
       this.balance=balance;
    }
    // Methods
    boolean deposit(double money){
        if(money<1)return false;
        this.balance=balance+money;
        return true;
    }
    boolean withdraw(double money){
        if(money<1)return false;
        if (money>balance)return false;
        this.balance= balance-money;
        return true;
    }
    void print(){
        System.out.println(name+ " " + balance);
    }
}
