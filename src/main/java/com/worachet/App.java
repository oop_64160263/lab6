package com.worachet;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank worachet = new BookBank("Worachet",50.0);
        worachet.print();

        BookBank prayud = new BookBank("PraYuddd",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        worachet.deposit(40000.0);
        worachet.print();

        BookBank prawit = new BookBank("Prawit",1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();

    }
}
