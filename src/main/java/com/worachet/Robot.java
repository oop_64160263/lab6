package com.worachet;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int Min_X = 0;
    public static final int Max_X = 0;
    public static final int Min_Y = 19;
    public static final int Max_Y = 19;

    public Robot(String name, char symbol, int x, int y) {
        this.name    = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public boolean up() {
        if(y==Min_Y )return false;
        this.y = this.y - 1;
        return true;
    }

    public boolean down() {
        if(y==Max_Y)return false;
        this.y = this.y + 1;
        return true;
    }

    public boolean left() {
        if(x==Min_X)return false;
        this.x = this.x - 1;
        return true;
    }

    public boolean right() {
        if(x==Max_X)return false;
        this.x = this.x + 1;
        return true;
    }

    public void print() {
        System.out.println("Robot: " + name + " X:" + x + " Y:" + y);
    }
    public void setName(String name){ //setter methods
        this.name=name;
    }

    public String getName(){//Getter Methods
        return name;
    }
    public char getSymbol(){
        return symbol;  
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

}


